use strict;
use warnings;

use utf8;
use JSON;
use File::Slurp;

my @words = @{
    JSON::decode_json(
        File::Slurp::read_file('words_rus_big.json')
    )
};
@words = grep { /^[^-]{3,}$/ } @words;

my $out = join(",", (map { "\"$_\"" } @words));

utf8::encode($out);

print $out;
print "\n";
